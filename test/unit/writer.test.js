/* global expects */
require('../bootstrap');
const {reader,writer} = require('../../console.io');
const {captureStream} = require('../../captureStream');


describe('writer test', () => {
    it('should build a string and write to stdout', () => {
        writer.build('Hello');
        writer.build('World!');

        hook = captureStream(process.stdout);
        writer.write();
        hook.unhook(); // unhook to see output of test.

        expect(hook.captured()).to.equal('Hello World!\n');
    });

    it('should build the string and write it, then clear it', () => {
        writer.reset();

        hook = captureStream(process.stdout);
        writer.write();
        hook.unhook(); // unhook to see output of test.

        expect(hook.captured()).to.equal('\n');
    });

    it('should exit program', () => {
        reader.on('close', () => {
            console.log('Have a great day!');
        });

        hook = captureStream(process.stdout);
        reader.close();
        hook.unhook(); // unhook to see output of test.

        expect(hook.captured()).to.equal('Have a great day!\n');
    });
});
