const {writer, reader} = require('../../console.io');
module.exports.exit = function() {
    reader.close();
}
module.exports.goodbye = function() {
    reader.close();
}
