const exec = require('child_process').exec;
const {writer, reader} = require('../../console.io');
module.exports.test = function() {
  exec('yarn test', function (error, stdout, stderr) {
      if (error) {
          console.log(error);
          process.exit(1);
      }

      if (stderr) {
          console.log(stderr);
          process.exit(1);
      }

      console.log(stdout);
      reader.prompt();
  });
}
