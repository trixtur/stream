var fs = require('fs');
var path = require('path');
module.exports.commandHandler = {
}

actionDir = process.cwd() + '/commandHandler';

const isDirectory = source => fs.lstatSync(source).isDirectory();
const getDirectories = source =>
  fs.readdirSync(source).map(name => path.join(source, name)).filter(isDirectory);

var basename = path.basename(__filename);

getDirectories(actionDir).forEach(function(actions) {
    var actions = require(actions);
    Object.keys(actions).forEach(function(action) {
        module.exports.commandHandler[action] = actions[action];
    });
});
