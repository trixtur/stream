const {writer, reader} = require('./console.io');
const {commandHandler} = require('./commandHandler');

reader.prompt();
reader.on('line', (line) => {
  const command = line.trim().toLowerCase();
  if (typeof commandHandler[command] !== 'undefined')
    commandHandler[command]();
  else {
    console.log(`Say what? I might have heard '${line.trim()}'`);
    reader.prompt();
  }
}).on('close', () => {
  console.log('Have a great day!');
  process.exit(0);
});
