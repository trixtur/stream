'use strict';
const readline = require('readline');

const myReadLine = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: '> ',
});

module.exports.writer = {
    message: '',
    build: function(message) {
        if (!this.message) this.message += message;
        else this.message += ' ' + message;
    },
    reset: function() {
        this.message = '';
    },
    write: function() {
        console.log(this.message);
    }
};

module.exports.reader = myReadLine;
